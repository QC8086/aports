# Maintainer: Milan P. Stanić <mps@arvanta.net>
# **** temporary aport till all is upstreamed *****
pkgname=u-boot-asahi
pkgver=2023.04_p1
pkgrel=0
pkgdesc="u-boot bootloader for Apple Silicon Macs"
url="https://github.com/AsahiLinux/u-boot"
arch="aarch64"
license="GPL-2.0-or-later OFL-1.1 BSD-2-Clause BSD-3-Clause eCos-2.0 IBM-pibs
	ISC LGPL-2.0-only LGPL-2.1-only X11"
options="!check" # no tests
depends="m1n1"
triggers="$pkgname.trigger=/boot/dtbs-asahi/apple:/usr/share/m1n1/m1n1.bin:/usr/share/u-boot-asahi/u-boot-nodtb.bin"

makedepends="$depends_dev
	bash
	bc
	bison
	dtc
	flex
	gnutls-dev
	linux-headers
	openssl-dev
	py3-setuptools
	python3-dev
	swig
	util-linux-dev
	"
source="https://github.com/AsahiLinux/u-boot/archive/refs/tags/asahi-v${pkgver/_p/-}.tar.gz
	update-u-boot-asahi
	apritzel-first5-video.patch
	mps-u-boot-ter12x24.patch
	"
builddir="$srcdir"/$pkgname-v${pkgver/_p/-}

prepare() {
	default_prepare
	make apple_m1_defconfig
	scripts/config --file .config --disable "VIDEO_FONT_8X16"
	scripts/config --file .config --enable "VIDEO_FONT_TER12X24"
}

build() {
	make
}

package() {
	mkdir -p $pkgdir/usr/share/$pkgname/dtb
	mkdir -p $pkgdir/usr/sbin
	install -m644 "$builddir"/u-boot-nodtb.bin "$pkgdir"/usr/share/$pkgname/
	cp "$builddir"/arch/arm/dts/t[86]*.dtb "$pkgdir"/usr/share/$pkgname/dtb/
	install -m744 "$srcdir"/update-u-boot-asahi "$pkgdir"/usr/sbin/
}

sha512sums="
8100c95df97ec5278de849ea86b97b128bb24e505f790bfb751320e35dae519cf3aea3b2cace3efc707b6d79ee7c74914a4527bc0abd5395cdc4407ce2ad8f38  asahi-v2023.04-1.tar.gz
e096055da0c0fbd2b072098fe47f434c7798ab4cd9a602555e0750656feb5b7d1a0d6f8a20380afbbb53dafabd2a0455a7146c0f69e33c404c79028ad9e7a394  update-u-boot-asahi
d15d786bcdbf79bb1db88f841f9d5d958c2da7140022f338c5778e11873e2f7d1726df6656a5d59390c3db78291fcbf5977353970e25934c6d6b55e8b12bad22  apritzel-first5-video.patch
d8ff48d920d12c6a2a3d514f1aed2772bd3943853d4ff93a3e9b6bc4f3b333ac5edd00123a7d61071ac1838c1c4f831d889960f940faca37b38a000434027030  mps-u-boot-ter12x24.patch
"

# Contributor: Grigory Kirillov <txgk@bk.ru>
# Maintainer: Grigory Kirillov <txgk@bk.ru>
pkgname=qucs-s
pkgver=1.0.2
pkgrel=0
pkgdesc="Fancy graphical user interface for a number of popular circuit simulation engines"
url="https://github.com/ra3xdh/qucs_s"
# ppc64le, riscv64, s390x: blocked by octave
arch="all !ppc64le !riscv64 !s390x"
license="GPL-2.0-only"
options="!check" # no test suite
depends="
	ngspice
	octave
	"
makedepends="
	cmake
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
source="https://github.com/ra3xdh/qucs_s/archive/$pkgver/qucs_s-$pkgver.tar.gz"
langdir="/usr/share/qucs-s/lang"
builddir="$srcdir/qucs_s-$pkgver"

build() {
	cmake -B builddir -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release
	cmake --build builddir
}

package() {
	DESTDIR="$pkgdir" cmake --install builddir
}

sha512sums="
69b91d4b2fc420e540ddca7a4993ed1177d3f8bbe6769ecfbc9a134a2e3dcb5a79e207369d083734557c16a0d0ab673b30141b27994aada1d58d08842730a6a9  qucs_s-1.0.2.tar.gz
"

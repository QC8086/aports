# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kimap
pkgver=23.04.1
pkgrel=0
pkgdesc="Job-based API for interacting with IMAP servers"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kio-dev
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	cyrus-sasl-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kmime-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kimap-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# loginjobtest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "loginjobtest" -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8651f85e0db7732c006395f8272ae1fa49a7731f29a0c13dead09742a65371e0ca438e0ed65125b657aed65976bc0bf1bc1c5664b6f9a02e36aa840f10f04705  kimap-23.04.1.tar.xz
"

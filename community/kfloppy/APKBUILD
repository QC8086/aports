# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kfloppy
pkgver=23.04.1
pkgrel=0
pkgdesc="A utility that provides a straightforward graphical means to format 3.5\" and 5.25\" floppy disks"
url="https://kde.org/applications/utilities/org.kde.kfloppy"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later"
depends="
	coreutils
	dosfstools
	e2fsprogs
	"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kfloppy-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
776cb732efcc12b7bd4e7cd6a65d64c345421787ee6e62e9be268a3ff65491d28f88bc0987d7e4c6b689c1eebd1d3302638047e3bc2c1fb8c35246479c7236ad  kfloppy-23.04.1.tar.xz
"

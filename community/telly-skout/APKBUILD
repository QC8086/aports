# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=telly-skout
pkgver=23.04.1
pkgrel=0
pkgdesc="Convergent TV guide based on Kirigami"
url="https://invent.kde.org/plasma-mobile/telly-skout"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
license="GPL-2.0-or-later AND LicenseRef-KDE-Accepted-GPL"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	ki18n-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/telly-skout-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5bee45f201304e76f836c493e1c0799ccc78ad576c48ba2bea4381ee12047db20698f8c5e5f843eebf46e2869031cef6b315517834e7895a82b257ff3c2e7516  telly-skout-23.04.1.tar.xz
"

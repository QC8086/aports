# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcrash
pkgver=5.106.0
pkgrel=0
pkgdesc="Support for application crash analysis and bug report from apps"
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcoreaddons-dev
	kwindowsystem-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcrash-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# kcrashtest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kcrashtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
13a9449d92e56c97d7dfe081a4c8e0cba550007e6f81c1660a3b97c32be3accdf2b9246478616cf06a3894adc85514a88974486a251629af86cdd65ddb6ad6b6  kcrash-5.106.0.tar.xz
"

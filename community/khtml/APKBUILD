# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=khtml
pkgver=5.106.0
pkgrel=0
pkgdesc="The KDE HTML library, ancestor of WebKit"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND LGPL-2.1-only"
depends_dev="
	giflib-dev
	karchive-dev
	kcodecs-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjs-dev
	knotifications-dev
	kparts-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libjpeg-turbo-dev
	perl-dev
	qt5-qtbase-dev
	sonnet-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	gperf
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/khtml-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fe8632ffecb686578dc9d6e49651d5b81447b860051a11ddfdef8a6b9da96f139ceb1a9bfeb3e56ff0f371bd80e80c17575d7f8d3ab1806b27a7cbb14a5c91da  khtml-5.106.0.tar.xz
"

# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=cargo-make
pkgver=0.36.7
pkgrel=1
pkgdesc="Rust task runner and build tool"
url="https://github.com/sagiegurari/cargo-make"
# riscv64: TODO
# s390x: fails to build nix crate
arch="all !riscv64 !s390x"
license="Apache-2.0"
makedepends="cargo openssl-dev cargo-auditable"
subpackages="$pkgname-bash-completion"
source="https://github.com/sagiegurari/cargo-make/archive/$pkgver/cargo-make-$pkgver.tar.gz"
options="!check"  # FIXME: some tests are broken

_cargo_opts="--frozen --no-default-features --features tls-native"


prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build $_cargo_opts --release
}

check() {
	cargo test $_cargo_opts
}

package() {
	install -D -m755 -t "$pkgdir"/usr/bin/ \
		target/release/cargo-make \
		target/release/makers

	install -D -m644 extra/shell/makers-completion.bash \
		"$pkgdir"/usr/share/bash-completion/completions/makers
}

sha512sums="
6593666d37931a57e0464e8330a528234d0035ea7d461c2b31af25bf7e00d4d5ef9ed0e2d2daac04c307d2a356b43744ecd6336b2450cfeade7284e96933f9df  cargo-make-0.36.7.tar.gz
"

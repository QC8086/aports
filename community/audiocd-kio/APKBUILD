# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=audiocd-kio
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="Kioslave for accessing audio CDs"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	cdparanoia-dev
	extra-cmake-modules
	flac-dev
	kcmutils-dev
	kconfig-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	libkcddb-dev
	libkcompactdisc-dev
	libvorbis-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/audiocd-kio-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4a6047c4db024d7ec0fc9c942dd7d1f366290867753735d639d24047ed1a22e871e08c8f6837cdeaf4b5666a6487b3e0a8323ee50bcdd9dda76e35ba70ee3d21  audiocd-kio-23.04.1.tar.xz
"

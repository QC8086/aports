# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ki18n
pkgver=5.106.0
pkgrel=0
pkgdesc="Advanced internationalization framework"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-or-later)"
depends_dev="
	qt5-qtdeclarative-dev
	qt5-qtscript-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	doxygen
	graphviz
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ki18n-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# ki18n-klocalizedstringtest, kcountrytest, kcountrysubdivisiontest and kcatalogtest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(ki18n-klocalizedstring|kcountry|kcountrysubdivision|kcatalog)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8012b5cc1d78ae92b3077d978b6010e032183b6ce8ceb0ef4df928bb1d2f21be4fe91e584cf5bac40539cc9c11cc9e5f5973ab8333518cfca4d8119f31bdabd5  ki18n-5.106.0.tar.xz
"

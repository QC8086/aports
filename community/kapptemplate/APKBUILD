# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kapptemplate
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/development/org.kde.kapptemplate"
pkgdesc="Factory for the easy creation of KDE/Qt components and programs"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kapptemplate-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
20722baf5479712d8987e7ab18b866d489c8e644858cc1753926062a48fc990646721ad50364ea1849b364084042dd744f76dfd598986d033c3f2a64ffbfc5c6  kapptemplate-23.04.1.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=parley
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/education/org.kde.parley"
pkgdesc="Vocabulary Trainer"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kross-dev
	kxmlgui-dev
	libkeduvocdocument-dev
	libxml2-dev
	libxslt-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	qt5-qtwebengine-dev
	samurai
	sonnet-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/parley-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dc184aa8d0a4c6288a6f0f9bd7062491fd2b07acdb11938ac6700f08a494aa1b8c5e4cea74cb7b12c3202a2d73f33ba4c967ba4b1f04650e439e8bd7ecfc3154  parley-23.04.1.tar.xz
"

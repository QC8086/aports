# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kbackup
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kbackup"
pkgdesc="An application which lets you back up your data in a simple, user friendly way"
license="GPL-2.0-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libarchive-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kbackup-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5dcbfd083ff68f94cb31ef553b99bc617c43f57c0d56aeb090103e2d28ea9c360ad6973520c0c096c09a0c6827bea4e4ee5dd6a2baa8168b72f7d729d97022f1  kbackup-23.04.1.tar.xz
"

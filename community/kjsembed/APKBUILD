# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kjsembed
pkgver=5.106.0
pkgrel=0
pkgdesc="JavaScript bindings for QObject"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtsvg-dev kjs-dev ki18n-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev kdoctools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kjsembed-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
6dba5cf149cfaaf828dd382c144bc86d43c092339bad75f8d2a03d9bd4863e72cc53b6643af61a1440934bb68c545608ce24e1425c5da11849debb2b34dc2b1c  kjsembed-5.106.0.tar.xz
"

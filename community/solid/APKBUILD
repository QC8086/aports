# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=solid
pkgver=5.106.0
pkgrel=0
pkgdesc="Hardware integration and detection"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="qt5-qtdeclarative-dev"
makedepends="$depends_dev
	extra-cmake-modules
	bison
	doxygen
	eudev-dev
	flex-dev
	qt5-qttools-dev
	samurai
	udisks2-dev
	upower-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/solid-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# solidmttest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "solidmttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
28edc90c9062aeba0443b8a4f238c76f65b66aae8ba092d2ee0a6d72bcc6503c86da245560d3f5efdf2ed87c3195114fdf4ff4a4a26faed1e8deae2cfa77d24b  solid-5.106.0.tar.xz
"

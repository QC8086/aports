# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=bluez-qt
pkgver=5.106.0
pkgrel=0
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
pkgdesc="Qt wrapper for Bluez 5 DBus API"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev"
makedepends="$depends_dev extra-cmake-modules doxygen graphviz qt5-qttools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/bluez-qt-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Multiple tests either hang or fail completely

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
93419179c793156861bd32109d07cb7ca7bcc7aaca59db737b527b8a86de54e81d34bd0b53f982d49364974dc5e083079b2cf140418d8e70336d4c853e6431c2  bluez-qt-5.106.0.tar.xz
"

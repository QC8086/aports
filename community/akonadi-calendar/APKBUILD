# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-calendar
pkgver=23.04.1
pkgrel=0
pkgdesc="Akonadi calendar integration"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by kmailtransport -> libkgapi -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev>=$pkgver
	akonadi-dev>=$pkgver
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kio-dev
	kmailtransport-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-calendar-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Broken

replaces="kalendar>1.0.0"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
c5eebb78ad0edf70b6299278cfcb1ad9718bbdcabad7bb688a920b23c7ca6ba06c1080ecfb1923c8835f092fce6d3ef30a2409f199bffb984f048184330b0a2c  akonadi-calendar-23.04.1.tar.xz
"

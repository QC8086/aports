# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kjumpingcube
pkgver=23.04.1
pkgrel=0
pkgdesc="A simple dice driven tactical game"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://apps.kde.org/kjumpingcube/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kjumpingcube-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6e68d3cfdbfacc669f32938e557c89e3803ac576e94a92e28a75763d6fe1eae9b0e2c6cf744467d53f9657e8d354ccc398bdb5bdcb9718fd0a95402a91e942d4  kjumpingcube-23.04.1.tar.xz
"

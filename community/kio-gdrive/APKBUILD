# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kio-gdrive
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> libkgapi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KIO_GDrive"
pkgdesc="KIO Slave to access Google Drive"
license="GPL-2.0-or-later"
depends="
	kaccounts-providers
	signon-plugin-oauth2
	signon-ui
	"
makedepends="
	extra-cmake-modules
	kaccounts-integration-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	libkgapi-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service//$pkgver/src/kio-gdrive-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1d10fd304860b26e7cd02065d91f1f2690a1d8a66ad475475e618b2d31b86d54acd2c1cf51f8360d3666aa79f9f7a63f6a3c4998ba1bacd09b810c535880f664  kio-gdrive-23.04.1.tar.xz
"

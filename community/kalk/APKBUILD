# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalk
pkgver=23.04.1
pkgrel=0
pkgdesc="A powerful cross-platfrom calculator application"
arch="all !armhf" # Blocked by qt5-qtdeclarative
url="https://invent.kde.org/plasma-mobile/kalk"
license="GPL-3.0-or-later"
depends="qt5-qtfeedback"
makedepends="
	bison
	extra-cmake-modules
	flex
	gmp-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kirigami2-dev
	kunitconversion-dev
	mpfr-dev
	qt5-qtbase-dev
	qt5-qtfeedback-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalk-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	cd build

	# inputmanagertest_de is broken, https://invent.kde.org/plasma-mobile/kalk/-/issues/25
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "inputmanagertest_de|knumbertest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6816820f08eea9418dff52ea50386d7e35c5d0e0edbe751b69f458a6538fec184864d555a77a2a4301dac6e1bd2f83d87602a8d9aeb5156883e419e1e4fca26a  kalk-23.04.1.tar.xz
"

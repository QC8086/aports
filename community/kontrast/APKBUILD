# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kontrast
pkgver=23.04.1
pkgrel=0
pkgdesc="Tool to check contrast for colors that allows verifying that your colors are correctly accessible"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/accessibility/kontrast"
license="GPL-3.0-or-later AND CC0-1.0"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontrast-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c19918e37af107c6b3f9f7452e1410e18c7672df0a4f6f2890b93621dbfc445ba5a99e8c1a70c9a8b45582969fcc35ea9be93dc8b2df81c6212dcb649f78899e  kontrast-23.04.1.tar.xz
"

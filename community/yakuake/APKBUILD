# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=yakuake
pkgver=23.04.1
pkgrel=0
arch="all !armhf"
url="https://kde.org/applications/system/org.kde.yakuake"
pkgdesc="A drop-down terminal emulator based on KDE Konsole technology"
license="GPL-2.0-only OR GPL-3.0-only"
depends="konsole"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev qt5-qtx11extras-dev karchive-dev kconfig-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev kglobalaccel-dev ki18n-dev kiconthemes-dev kio-dev knewstuff-dev knotifications-dev knotifyconfig-dev kparts-dev kwidgetsaddons-dev kwindowsystem-dev kwayland-dev samurai"
source="https://download.kde.org/stable/release-service/$pkgver/src/yakuake-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
bc5dd46d80b5a857fa2b1c53cac05402f5207d645ed045f0558e1d7d0a5e785b4ca1e0a2f3256d0d42e54713eda70d61f1cd062e782be4460fd3045663b8a1ba  yakuake-23.04.1.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkexiv2
pkgver=23.04.1
pkgrel=0
pkgdesc="A library to manipulate pictures metadata"
url="https://www.kde.org/applications/graphics"
arch="all !armhf" # extra-cmake-modules
license="GPL-2.0-or-later"
makedepends="extra-cmake-modules qt5-qtbase-dev exiv2-dev samurai"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkexiv2-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
64fd820c376cdcb081599deea9a7817f48bf72aa7eed6bd710a0b6fad22a527e62e8f405787a506fe7bc7910f95acef232ba482d670d51240907013b8b106b6d  libkexiv2-23.04.1.tar.xz
"

# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=lego
pkgver=4.12.0
pkgrel=0
pkgdesc="Let's Encrypt client and ACME library written in Go"
url="https://github.com/go-acme/lego"
license="MIT"
arch="all !s390x" # tests fail due to network timeouts
options="net chmod-clean" # tests need network access: https://github.com/go-acme/lego/issues/560
depends="ca-certificates"
makedepends="go libcap-utils"
checkdepends="tzdata"
source="https://github.com/go-acme/lego/archive/v$pkgver/lego-$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

case "$CARCH" in
	aarch64)
		# TestChallengeWithProxy/matching_X-Forwarded-Host_(multiple_fields)
		options="$options !check"
		;;
	ppc64le)
		# timeout on doing stuff with a server halfway across the world
		options="$options !check"
		;;
esac

build() {
	go build -v -ldflags "-X main.version=$pkgver" -o ./bin/lego ./cmd/lego
}

check() {
	go test ./...
}

package() {
	install -Dm755 ./bin/lego "$pkgdir"/usr/bin/lego
	setcap cap_net_bind_service=+ep "$pkgdir"/usr/bin/lego
}

sha512sums="
d36708312298cde3573697299d3753ecfabd960196144be86b269bd2c2da6837d25909814476dda6ff6f9bc232741f716579327dc44fc20450343db7d0057dcc  lego-4.12.0.tar.gz
"

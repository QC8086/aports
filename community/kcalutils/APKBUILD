# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcalutils
pkgver=23.04.1
pkgrel=0
pkgdesc="The KDE calendar utility library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://api.kde.org/kdepim/kcalutils/html"
license="LGPL-2.0-or-later"
depends_dev="
	grantlee-dev
	kcalendarcore-dev
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kwidgetsaddons-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcalutils-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kcalutils-testincidenceformatter and kcalutils-testtodotooltip are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kcalutils-test(incidenceformatter|todotooltip|dndfactory)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
681945660d1c8b4fd7ea87c510de3ac137d51ca65dcfe80df8179cb167ef55caaeddca4ea9f760d4520a3b4be06794a2b04953a0143120a2ac6b8ce7af68cb26  kcalutils-23.04.1.tar.xz
"

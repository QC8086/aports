# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=neochat
pkgver=23.04.1
pkgrel=1
pkgdesc="A client for Matrix, the decentralized communication protocol"
url="https://invent.kde.org/network/neochat/"
# armhf blocked by extra-cmake-modules
# riscv64 and s390x blocked by qqc2-desktop-style
arch="all !armhf !s390x !riscv64"
license="GPL-2.0-or-later AND GPL-3.0-only AND GPL-3.0-or-later AND BSD-2-Clause"
depends="
	kirigami-addons
	kirigami2
	kitemmodels
	kquickimageeditor
	qqc2-desktop-style
	qt5-qtquickcontrols
	"
makedepends="
	cmark-dev
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kirigami2-dev
	kitemmodels-dev
	knotifications-dev
	kquickimageeditor-dev
	libquotient-dev
	qcoro-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qtkeychain-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	sonnet-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/neochat-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
be036156e1facac03b9846110d7b7c8d119f7ceefdb8597eec7fefc62cb90623a235195f58bc463c7412e847c5a1add93374ed98d7499e4373b506ea12cb11be  neochat-23.04.1.tar.xz
"

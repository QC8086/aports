# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kactivities
pkgver=5.106.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
pkgdesc="Core components for the KDE's Activities"
url="https://community.kde.org/Frameworks"
license="GPL-2.0-or-later AND LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="qt5-qtbase-sqlite"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	kwindowsystem-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	boost-dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bf1c8fd2745de4a6c7a9afaef1553ac76bf861d4a9479e6707e46839e9da5ccd1d92225658c9d6d555354b14c2243cc2aedfbb2714170fbc1a6f8abae7909a09  kactivities-5.106.0.tar.xz
"

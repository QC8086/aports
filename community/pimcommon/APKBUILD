# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=pimcommon
pkgver=23.04.1
pkgrel=1
pkgdesc="Common lib for KDEPim"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url='https://community.kde.org/KDE_PIM'
license="GPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev
	akonadi-dev
	karchive-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kimap-dev
	kio-dev
	kitemmodels-dev
	kjobwidgets-dev
	kmime-dev
	knewstuff-dev
	kpimtextedit-dev
	kservice-dev
	ktextaddons-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdepim-dev
	purpose-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/pimcommon-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="net" # net required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
57b50d703e3b8192558a8cd3d9eacce0e85a3d032c8871e31cb3d835fcb7b5eb87363f379f1ef39853a454b245f38a4c351dd481721ce52eee2a2acc71d6a568  pimcommon-23.04.1.tar.xz
"

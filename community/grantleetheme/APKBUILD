# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=grantleetheme
pkgver=23.04.1
pkgrel=0
pkgdesc="KDE PIM mail related libraries"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by knewstuff
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	grantlee-dev
	ki18n-dev
	knewstuff-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/grantleetheme-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# grantleethemetest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "grantleethemetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
0c412f1006d5a7661ca0b4058533ff74948cb4864e7c937ac8e8d7f06c1eb52969321ef4a823540e47b9251a7ff607b2cfb6912f0ec63ff8e4ca56a77ef0f7a5  grantleetheme-23.04.1.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalzium
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/kalzium/"
pkgdesc="Periodic Table of Elements"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	eigen-dev
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	knewstuff-dev
	kparts-dev
	kplotting-dev
	kunitconversion-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	solid-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalzium-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1fb48e7c187e346a3e6ec987cd1ed474cbb7029b9f200109cb6449912932ed53ad1391117e759cdb6ea72c85abb8df81472a62bebf741fc57f5cccfcbcc8248b  kalzium-23.04.1.tar.xz
"
